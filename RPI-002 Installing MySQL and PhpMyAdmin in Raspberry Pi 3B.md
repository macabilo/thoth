**Installing MySQL and PHPMyAdmin in Raspberry Pi 3**

Type the following commands to be able to install a copy of MySQL Database and 
PhpMyAdmin interface in your Raspberry Pi! This guide was last updated on 
April 7, 2022.

* `sudo apt-get update && sudo apt-get upgrade`
* `sudo apt-get install mariadb-server mariadb-client php-mysql phpmyadmin`

When installing **phpmyadmin**, you will be prompted with options such as the 
selection of web server to run phpmyadmin (**apache** or **lighttpd**). When 
prompted, select `apache2`.

If you will be asked by the installer if you want to configure phpmyadmin
automatically, just answer `yes`. But if you know your way around the configuration
files, feel free to say `no`.

Afterwards, you will be asked to input a password for the phpmyadmin account.

Edit apache configuration to include phpmyadmin:

* `sudo nano /etc/apache2/apache2.conf`

Then at the bottommost part of the file type in:

* `Include /etc/phpmyadmin/apache.conf`

Save the file and restart apache:

* `sudo /etc/init.d/apache2 restart`

Then you are done! You can login using default root by executing `sudo mysql --user=root mysql`

**Granting Remote Access to MySQL Database**

With phpmyadmin installed, we can configure our databases to be accessible from 
another host in the same local network. First, login to *root* account.

* `sudo mysql --user=root mysql`

Grant any account from any host access to all databases in RPI by
executing the following query:

* `GRANT ALL PRIVILEGES ON *.* TO '<username>'@'%' IDENTIFIED BY '<password>' WITH GRANT OPTION;`
* `FLUSH PRIVILEGES;`

Replace `*.*` with a specific database name if you wish to share only specific
databases. Replace `%` with a host name or IP address if you want only a certain
remote host to access your database remotely.

You can check if you are successful by opening the database from another host.
Get the IP address of your RPI:

* `hostname -I`

Open a browser and type on the address bar:

* `<ip_address>/phpmyadmin`

A login page should appear. If not, something was wrong during the setup. Login
using the *username* and *password* you configured earlier in remote access grant.


If you have any questions or clarifications on this installation guide, reach me
through the following contact information:
* cabilo.markanthony@gmail.com
