**Setting Up SSH and WiFi Connection Using Another Computer**

Remove the microSD from the RPi. It is expected that the RPi at this point has a running OS. Plug the microSD
into a computer. If the computer prompts to reformat the disk, click "Cancel". DO NOT FORMAT THE DISK.

Open the "boot" directory. The following steps will not work if you are not using headless RPi.

In the "boot" directory, create a new text document. Make sure that you can see the full file extension. 
Rename the said file to `ssh`. Make sure that it does not have a file extension.

Create another text document. Rename the file as `wpa_supplicant.conf`. Make sure that the resulting file type
is an *conf* file.

Inside this file, type the following:

 `ctrl_interface=/var/run/wpa_supplicant group=netdev`  
 `network={`  
 `ssid="yourSSIDname"`   
 `psk="yourPasskey"`  
 `}`  

Save the conf file and eject the microSD. Plug it into your Raspberry Pi and boot it up. You can now remotely
access the RPi through the network you specified.
