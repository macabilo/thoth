**Installing TinyOS in Raspberry Pi 3B**

Type the following commands to be able to install a copy of the TinyOS platform
into your RPi and start developing wireless sensor network applications! This
guide was last updated on February 4, 2018.

* `sudo apt-get update`
* `sudo apt-get install emacs gperf bison flex git automake`
* `sudo reboot`

Log in to your RPi again and continue with the following installations:
* `sudo apt-get install build-essential openjdk-8-jdk openjdk-8-jre
python2.7 python2.7-dev avarice avr-libc msp430-libc avrdude binutils-avr
binutils-msp430 gcc-avr gcc-msp430 gdb-avr subversion graphviz python-docutils
checkinstall`

Take note that some utilities installed from step 4 might get updated over time.
Python and java may have newer versions over time. If you are running a headless
RPI (no desktop or GUI, only terminal), change `openjdk-8-jdk` to `openjdk-8-jdk-headless`
and `openjdk-8-jre` to `openjdk-8-jre-headless`.

**Update (May 2018):** Errors might appear for the openjdk-* packages saying
*missing 'server' JVM at $directory*

To solve this, go to the specified $directory, which may be at
`/usr/lib/jvm/java-8-openjdk-armhf/jre/lib/arm/`

do: 
1. `cd /usr/lib/jvm/java-8-openjdk-armhf/jre/lib/arm/`
2. `sudo mkdir server`
3. `sudo cp -r client server`

Then perform a reboot

**end of May 2018 update**

* `sudo reboot`

Log in again to your RPi and continue with the following:

* `git clone https://github.com/tinyos/nesc.git`
* `cd nesc/`
* `./Bootstrap`
* `./configure`
* `make`  
**Update May 2018:** if the results of the `make` instruction return errors, execute `make clean` to undo the compilation. Run `sudo apt-get install openjdk-8-jdk-headless openjdk-8-jre-headless --fix-missing` (remove `headless` if you are not in headless mode). The dpkg should affect ca-certificates-java:armhf.
Once successful, repeat `make` and continue.
**end update**
* `sudo make install`

At this point, you have installed **nesc**, which is the programming language
for the TinyOS platforms.
* `cd`
* `git clone https://github.com/tinyos/tinyos-main.git`
* `sudo nano ~/tinyos-main/tinyos.env`

An empty file will open and you will write the following lines into the file:

`export TOSROOT="~/tinyos-main"`  
`export TOSDIR="$TOSROOT/tos"`  
`export CLASSPATH=$CLASSPATH:$TOSROOT/support/sdk/java/tinyos.jar:.`  
`export MAKERULES="$TOSROOT/support/make/Makerules"`  
`export PYTHONPATH=$PYTHONPATH:$TOSROOT/support/sdk/python`  
`echo "Setting up TinyOS on source path $TOSROOT"`

Once this is completed. Press `ctrl+x` and save file.

* `sudo nano ~/.bashrc`

Once the *bashrc* file opens, go to the bottom most part of the file and write:
`source ~/tinyos-main/tinyos.env`

Remember that the macros or path variables defined here can only be used once 
either the terminal or the RPI is rebooted (`sudo reboot`)

* `sudo chmod -R 777 tinyos-main/`
* `cd tinyos-main/tools`
* `./Bootstrap`
* `./configure`
* `make`
* `sudo make install`

Copy c, java, and python folders to the directory specified in the environment
file *tinyos.env*.
* `cp -R $TOSROOT/tools/tinyos/c $TOSROOT/support/sdk`
* `cp -R $TOSROOT/tools/tinyos/java $TOSROOT/support/sdk`
* `cp -R $TOSROOT/tools/tinyos/python $TOSROOT/support/sdk`

* `sudo cp $TOSROOT/tools/tinyos/jni/serial/libtoscomm.so /usr/lib`
* `sudo cp $TOSROOT/tools/tinyos/jni/env/libgetenv.so /usr/lib`

Connect any tinyos-compatible device (e.g. telosB) to an RPI port and enter the
following command:
* `sudo chmod 777 /dev/ttyUSB0`

You're done.

**Side notes**
Sometimes, when uploading a tinyos code to a device for the first time,
*tos-bsl* will return an error regarding a missing python's *serial* module.

To solve this little problem, do the following:
* Install pip: `sudo apt-get install python-pip`
* Install pyserial: `sudo pip install pyserial`


If you have any questions or clarifications on this installation guide, reach me
through the following contact information:
* cabilo.markanthony@gmail.com
